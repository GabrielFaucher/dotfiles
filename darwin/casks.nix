{ }:

[
  # Development Tools
  "homebrew/cask/docker"
  "insomnia"
  "ngrok"
  "visual-studio-code"
  "wireshark"

  # Communication Tools
  "discord"
  "loom"
  "notion"
  "zoom"

  # Utility Tools
  "appcleaner"
  "syncthing"

  # Entertainment Tools
  "steam"
  "vlc"

  # Productivity Tools
  "raycast"

  # Browsers
  "google-chrome"

  # AI
  "diffusionbee"
]

