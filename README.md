# dotfiles

These are my dotfiles for configuring my development enviorent across different types of machines.
It includes support for MacOS and Ubuntu on WSL.

## Getting Started

Follow the platform-specific instructions below to get started with this configuration.

*If you're not me, note: these are configurations tailored to my own development workflow, requirements and style. Proceed with care.*

### Darwin / MacOS

The instructions below will show you how to setup these dotfiles for a MacOS system using `nix-darwin`.
It also has support for declarative `brew` configuration.

#### 1. Install XCode dependencies

`xcode-select --install`

#### 2. Install Nix

`curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install`

#### 3. Clone and install configuration

`git clone git@gitlab.com:gabrielfaucher/dotfiles.git ~/Developer/projects/dotfiles/`

`cd ~/Developer/projects/dotfiles/ && ./bin/build`

#### 4. Enable global flake access

`nix registry add system ~/Developer/projects/dotfiles/`

### WSL

The instructions below will show you how to setup these dotfiles for a WSL system using a standalone `home-manager` installation.

Important: make sure you're Windows installation is as updated as possible and has the latest WSL version. Make sure you click _check online for updates_ in Windows Update to fetch _all_ the latest updates. You can check this by trying the `wsl --version` command in Powershell.
If this doesn't exist and if it prints the help message instead, _you're not up to date_.\*

#### Install WSL with a Linux distribution

Follow all the instructions in the link below to install WSL. Install the Linux distribution of your choice.

*Note: I'd suggest picking Alpine as a distribution, unless you need `systemd` support. In that case, use Ubuntu.*
*I've added Alpine-specific instructions which may or may not apply to other distributions.*

`https://learn.microsoft.com/en-us/windows/wsl/install`

#### Optional: VPN support

Most corporate Windows environments make use of an always-on VPN to secure internet traffic.
This causes issues for WSL, as it messes up it's DNS servers. Run the `./bin/scripts/vpn-dns.sh` script to provide you with internet access to get started.

Link the VPN DNS script to a `/etc/init.d` to expose it to the `service` command:
`sudo ln -s ./bin/scripts/vpn-dns.sh /etc/init.d/vpn-dns.sh`

Link the WSL configuration file that autostarts the script:
`sudo ln -s ./bin/config/wsl.conf /etc/wsl.conf`

*Note: If you restart your PC after this and you don't have internet access in WSL _you're PC is not up to date_.*
*I've had this issue, and WSL just fails to execute the script silently.*

#### Alpine: Install build dependencies on Alpine

I've picked Alpine because I really love slim and non-bloated distros. Alpine is _so_ slim however, that it doesn't have the requirements build tools installed by default.
Execute the commands below to rectify this.

```
# Change user to root and install `sudo`
su -
apk add --no-cache build-base shadow 

# Enable group `wheel` to use `sudo` (this is convention)
echo '%wheel ALL=(ALL) ALL' > /etc/sudoers.d/wheel

# Add user to the `wheel` group
adduser <USERNAME> wheel 

# Set password for your user. This is not set by default in Alpine-WSL
passwd <USERNAME>

exit
```

#### Install Nix & Home Manager (standalone)

```
# Install the Nix package manager using the official installer:
curl -L https://nixos.org/nix/install | sh

# Configure support for flakes
mkdir -p ~/.config/nix
ln -s ./bin/config/nix.conf ~/.config/nix/nix.conf

# Add the home-manager Nix channel
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update

# Install home-manager
nix-shell '<home-manager>' -A install
```

#### Clone and install configuration

*Note: this assumes you've got you're SSH keys set up. If this is not the case, do so now.*

```
git clone git@gitlab.com:gabrielfaucher/dotfiles.git ~/Developer/projects/dotfiles/
cd ~/Developer/projects/dotfiles/ && ./bin/build
```

## Inspiration

[Chris Bailey: Creating a minimal Nix development environment for WSL](https://cbailey.co.uk/posts/a_minimal_nix_development_environment_on_wsl)
