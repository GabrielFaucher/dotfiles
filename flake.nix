{
  description =
    "A flaked Nix configuration for a NixOS workstation and a MacOS system using nix & homebrew";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/master";
    home-manager.url = "github:nix-community/home-manager";
    devshell.url = "github:numtide/devshell";
    android-nixpkgs.url = "github:tadfisher/android-nixpkgs";
    nix-homebrew.url = "github:zhaofengli-wip/nix-homebrew";
    flake-utils.url = "github:numtide/flake-utils";
    chaotic.url = "github:chaotic-cx/nyx/nyxpkgs-unstable";
    nixos-wsl.url = "github:nix-community/NixOS-WSL";

    darwin = {
      url = "github:LnL7/nix-darwin/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    homebrew-core = {
      url = "github:homebrew/homebrew-core";
      flake = false;
    };

    homebrew-cask = {
      url = "github:homebrew/homebrew-cask";
      flake = false;
    };
  };
  outputs = { self, nixpkgs, flake-utils, home-manager, nix-homebrew
    , homebrew-core, homebrew-cask, darwin, android-nixpkgs, devshell, chaotic
    , nixos-wsl }@inputs:
    let
      linuxSystems = [ "x86_64-linux" ];
      darwinSystems = [ "aarch64-darwin" ];
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        config.allowUnfree = true;
      };
      forAllSystems = f:
        nixpkgs.lib.genAttrs (linuxSystems ++ darwinSystems) (system: f system);
      devShell = system:
        import ./shells { inherit nixpkgs android-nixpkgs system; };
    in {
      devShells = forAllSystems devShell;
      homeConfigurations."gabriel" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [ ./wsl/home.nix ];
      };
      darwinConfigurations = {
        "nixbook" = darwin.lib.darwinSystem {
          system = "aarch64-darwin";
          specialArgs = inputs;
          modules = [
            home-manager.darwinModules.home-manager
            nix-homebrew.darwinModules.nix-homebrew
            chaotic.homeManagerModules.default
            {
              nix-homebrew = {
                enable = true;
                user = "gabriel";
                taps = {
                  "homebrew/homebrew-core" = homebrew-core;
                  "homebrew/homebrew-cask" = homebrew-cask;
                };
                mutableTaps = false;
              };
            }
            ./darwin
          ];
        };

      };
    };
}
