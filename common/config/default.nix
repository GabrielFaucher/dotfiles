{ ... }:

{
  imports = [ ./fish.nix ./tmux.nix ./git.nix ];

  xdg = {
    enable = true;
    configFile."nvim" = { source = ./nvim; };
    configFile."alacritty/alacritty.toml" = { source = ./alacritty.toml; };
  };
}
