return {
	{
		"nvim-treesitter/nvim-treesitter-textobjects",
		dependencies = { "nvim-treesitter/nvim-treesitter", "NvChad/nvim-colorizer.lua" },
		config = function()
			require("colorizer").setup({})

			require("nvim-treesitter.configs").setup({
				highlight = { enable = true },
				sync_install = false,
				ignore_install = { "dart" },
				modules = {},
				auto_install = true,
				textobjects = {
					select = {
						enable = true,
						lookahead = true,
						keymaps = {
							["af"] = "@function.outer",
							["if"] = "@function.inner",
							["ac"] = "@class.outer",
							["ic"] = "@class.inner",
						},
					},
				},
				ensure_installed = {
					"bash",
					"comment",
					"css",
					"html",
					"javascript",
					"jsdoc",
					"jsonc",
					"lua",
					"markdown",
					"regex",
					"scss",
					"toml",
					"typescript",
					"yaml",
					"c_sharp",
					"cmake",
					"cpp",
					"diff",
					"dockerfile",
					"elixir",
					"elm",
					"erlang",
					"fsh",
					"gitcommit",
					"gitignore",
					"go",
					"haskell",
					"haskell_persistent",
					"http",
					"java",
					"latex",
					"luadoc",
					"make",
					"nix",
					"python",
					"sql",
					"ssh_config",
					"ninja",
					"ninja",
				},
				autotag = {
					enable = true,
				},
			})
		end,
	},
	{
		"rebelot/kanagawa.nvim",
		priority = 1000,
		config = function()
			require("kanagawa").setup({
				transparent = true,
				colors = {
					theme = {
						all = {
							ui = {
								bg_gutter = "none",
							},
						},
					},
				},
			})
			vim.cmd("colorscheme kanagawa")
		end,
	},
}
