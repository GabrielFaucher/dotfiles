{ ... }:

{
  programs.git = {
    enable = true;
    ignores = [ "*.swp" ];
    userName = "Gabriel Faucher";
    userEmail = "gabriel.faucher@axians.com";
    lfs = { enable = true; };
    extraConfig = {
      init.defaultBranch = "master";
      core = {
        editor = "nvim";
        autocrlf = "input";
      };
      pull.rebase = true;
      rebase.autoStash = true;
    };
  };
}
