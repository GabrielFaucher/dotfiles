{ pkgs }:

with pkgs; [
  vim
  wget
  neovim
  git
  gcc
  ripgrep
  grc
  nix-your-shell

  starship
]
