{ pkgs, ... }:
with pkgs;

let
  common-packages = import ../common/packages.nix { inherit pkgs; };
  system-packages = import ../common/system-packages.nix { inherit pkgs; };
in common-packages ++ system-packages
++ [ cypress git openssh nix-your-shell ripgrep xclip gcc zig tree-sitter ]
