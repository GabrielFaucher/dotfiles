{ pkgs, ... }:

{
  imports = [ ../common/config ];

  home = {
    username = "gabriel";
    homeDirectory = "/home/gabriel";
    stateVersion = "23.05";
    packages = pkgs.callPackage ./packages.nix { };
    sessionVariables = { EDITOR = "nvim"; };
  };

  programs.home-manager.enable = true;
}
